//
//  LoginViewController.swift
//  appyWP
//
//  Created by Ali on 10/02/2018.
//  Copyright © 2018 Focused on Fit LLC. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Toaster


class LoginViewController: UIViewController,FBSDKLoginButtonDelegate {

    @IBOutlet weak var FBLogin: FBSDKLoginButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        let facebookReadPermissions = ["public_profile", "email", "user_friends" ]
        FBLogin.delegate = self
        FBLogin.readPermissions = facebookReadPermissions
        if (FBSDKAccessToken.current() != nil) {
            print(FBSDKAccessToken.current().userID)
            // User is logged in, do work such as go to next view controller.
        }
        // Do any additional setup after loading the view.
    }

    @IBAction func confirmedAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "email , id, name, first_name, last_name , gender"]).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                print(result ?? "NO RESULT")
                
                if let data = result as? [String:Any] {
                    
                    var userFacebookData = data
                    let name =  "\(String(describing: userFacebookData["first_name"]!))" + " " +  "\(String(describing: userFacebookData["last_name"]!))"
                    
                    Toast(text: "Logged In as \(name)", duration: Delay.long).show()
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "MainNavigationController")
                    self.present(vc, animated: true, completion: nil)

                }
                else {
                    Toast(text: "No result found against this account.", duration: Delay.long).show()
                    print("No data retrieved from your account.")
                }
            }
            else {
                print(error?.localizedDescription ?? "Fault")
            }
        })
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Logout")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
