import Foundation
import UIKit
import CoreData

struct Posts {
    var postID: Int?
    var postContent: String?
    var postDate: String?
    var postFeaturedImageURL: String?
    var postLink: String?
    var postModified: String?
    var postTitle: String?
    var postAuthor: String?
    var postURL: String?
    
    init(post:NSDictionary) {
        
        postID = post["id"] as? Int
        postDate = post["date_gmt"] as? String
        postLink = post["link"] as? String
        postModified = post["modified"] as? String
        postURL = post["link"] as? String
        
        if let contentDictionary: NSDictionary = post["content"] as? NSDictionary {
            if let content = getRenderedContent(contentDictionary) {
                postContent = content
            }
        }
        
        if let titleDictionary: NSDictionary = post["title"] as? NSDictionary {
            if let title = getRenderedContent(titleDictionary) {
                postTitle = title
            }
        }
        
        if let embeddedContent = post["_embedded"] as? NSDictionary {

            // Author
            if let authorInfo = embeddedContent["author"] as? NSArray {
                if let authorDict = authorInfo[0] as? NSDictionary {
                    postAuthor = authorDict["name"] as? String
                }
                
            }
            
            if let curieFM = embeddedContent["wp:featuredmedia"] as? NSArray {
                postFeaturedImageURL = getFeaturedImage(mediaArray: curieFM)
            }
            
        }
        
    }
    
    init(managedObject: NSManagedObject?) {
        if let managedObject = managedObject {
            postID = managedObject.value(forKey: "postID") as? Int
            postDate = managedObject.value(forKey: "postDate") as? String
            postLink = managedObject.value(forKey: "postLink") as? String
            postModified = managedObject.value(forKey: "postModified") as? String
            postURL = managedObject.value(forKey: "postURL") as? String
            postContent = managedObject.value(forKey: "postContent") as? String
            postTitle = managedObject.value(forKey: "postTitle") as? String
            postAuthor = managedObject.value(forKey: "postAuthor") as? String
            postFeaturedImageURL = managedObject.value(forKey: "postFeaturedImageURL") as? String
        }
    }
    
    func getFeaturedImage(mediaArray: NSArray) -> String? {
        if let mediaArray = mediaArray[0] as? NSDictionary {
            if let mediaDetails = mediaArray["media_details"] as? NSDictionary {
                if let sizes = mediaDetails["sizes"] as? NSDictionary {
                    if let featuredMedia = sizes["\(featuredImageSize)"] as? NSDictionary {
                        if let mediaURL = featuredMedia["source_url"] as? String {
                            return mediaURL
                        }
                    }
                }
            }
        }
        return nil
    }
    func getRenderedContent(_ renderedDict:NSDictionary) -> String? {
        
        if let renderedValue = renderedDict["rendered"] as? String {
            return renderedValue
        }
        return nil
    }
    
}
