
import UIKit


// GENERAL SETUP

// Your website name (will appear in the Menu)
let websiteName = "Crypto Options"

// Your website URL
// Remember that you must install in your Wordpress site
// - WP REST API (https://wordpress.org/plugins/rest-api/)
// - WP REST API FIlter Plugin (https://github.com/WP-API/rest-filter)
let websiteUrl = "http://crypto-signals.xyz/"


// FEATURED IMAGE
// The images that you want to use of your wordpress articles: "medium", "large", "full"...
let featuredImageSize = "full"


// TEXT
// General text strings
let noMorePostsText = "End of posts"
let noConnectionText = "Could not connect"
let loadingPostsText = "Loading Posts..."


// ADS
// Choose if you want to show ads
// and insert your AdMob Unit Id
let showAds = false
let Product_ID = "KalimeraApps"
let ADMOB_UNITID = "ca-app-pub-7918763499019250/8388198470"


// THEME COLOR
// Set this variable to false for the Light (White and Orange) theme
let useDarkColor = true


// CATEGORIES MENU
//
// menu1show -> Is the text that will appear on the menu
// menu1catName -> is the url after your website name
//                 (i.e. if the url of your category Nissan is "http://mysite.com/cars/nissan"
//                  you have to set this variable to "cars/nissan")

let homeShow = "News"

let menu1show = "Analysis"
let menu1catName = "category/analysis/"

let menu2show = "Bitcoin news"
let menu2catName = "category/bitcoin-news/"

let menu3show = "Altcoin news"
let menu3catName = "category/altcoin-news/"

let menu4show = "Ethereum news"
let menu4catName = "category/ethereum-news/"

let menu5show = "Blockchain news"
let menu5catName = "category/blockchain-news/"

let menu6show = "Regulation News"
let menu6catName = "category/regulation_news/"

let menu7show = "Scams News"
let menu7catName = "category/scams_news/"

let menu8show = "Explained"
let menu8catName = "category/explained/"

let menu9show = "Events"
let menu9catName = "category/events/"

let menuBookmarkShow = "Bookmarked News"
let menuBookmarkCatName = "category/bookmarks/"




