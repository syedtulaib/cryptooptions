

import UIKit
import GoogleMobileAds
import Appodeal
import AppsFlyerLib
import CoreData

class NewsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, GADBannerViewDelegate,AppodealInterstitialDelegate {

    @IBOutlet var footerLoadingView: FooterLoadingView!
    @IBOutlet var footerLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var menuView: SpringView!
    @IBOutlet var btnMask: UIButton!
    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var lblLoadingPosts: UILabel!
    @IBOutlet var lblWebsiteName: UILabel!
    @IBOutlet var viewRoot: UIView!

    @IBOutlet var top1: NSLayoutConstraint!
    @IBOutlet var top2: NSLayoutConstraint!
    @IBOutlet var top3: NSLayoutConstraint!
    @IBOutlet var top4: NSLayoutConstraint!
    @IBOutlet var top5: NSLayoutConstraint!
    @IBOutlet var top6: NSLayoutConstraint!
    @IBOutlet var top7: NSLayoutConstraint!
    @IBOutlet var top8: NSLayoutConstraint!
    @IBOutlet var top9: NSLayoutConstraint!
    @IBOutlet var top10: NSLayoutConstraint!
    
    @IBOutlet var btnHome: UIButton!
    @IBOutlet var btnMenu1: UIButton!
    @IBOutlet var btnMenu2: UIButton!
    @IBOutlet var btnMenu3: UIButton!
    @IBOutlet var btnMenu4: UIButton!
    @IBOutlet var btnMenu5: UIButton!
    @IBOutlet var btnMenu6: UIButton!
    @IBOutlet var btnMenu7: UIButton!
    @IBOutlet var btnMenu8: UIButton!
    @IBOutlet var btnMenu9: UIButton!
    @IBOutlet var btnMenu10: UIButton!
    
    var appodealAdTimer: Timer?
    var postsArray: [Posts] = Array()
    var bookmarkedPostsArray: [NSManagedObject] = Array()
    var currentPage: Int = 1
    var totalPosts: Int = 0
    var totalPages: Int = 0
    var didAnimateCell:[IndexPath: Bool] = [:]
    let refreshControl = UIRefreshControl()
    var selectedCategory = ""
    var curIdx: Int = 0
    var stopLoad: Bool = false
    var adMobBannerView = GADBannerView()
    var adHeight = 50 as CGFloat
    var purchased = false
    var nonConsumablePurchaseMade = UserDefaults.standard.bool(forKey: "nonConsumablePurchaseMade")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Check if IAP
        if let cryptoptionsID = UserDefaults.standard.value(forKey: "cryptoptionsID"){
            purchased = cryptoptionsID as! Bool
        }else{
            UserDefaults.standard.set(false, forKey: "cryptoptionsID")
            purchased = false
        }
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        title = "News"
        
        menuView.layer.zPosition = 10
        setMenuNames()
        
        if UIDevice.current.userInterfaceIdiom != .pad  {
            formatiPhone()
        } else {
            formatiPad()
        }
        
        if useDarkColor {
            setColorsDark()
        }
        
//        //Appodeal
//        if !purchased{
//            Appodeal.setInterstitialDelegate(self)
//            Appodeal.showAd(AppodealShowStyle.interstitial, rootViewController: self)
//            AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: [AFEventParamContentId: "1234567", AFEventParamContentType: "category_a", AFEventParamRevenue: 0, AFEventParamCurrency: "USD"])
//        }
        
        getBlogPosts(currentPage)
        view.showHUD(view)
        
        IAPHandler.shared.fetchAvailableProducts()
        IAPHandler.shared.purchaseStatusBlock = {[weak self] (type) in
            guard let strongSelf = self else{ return }
            if type == .purchased {
                let alertView = UIAlertController(title: "", message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    UserDefaults.standard.set(true, forKey: "cryptoptionsID")
                    self?.purchased = true
                    
                    AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: [AFEventParamContentId: "1234567", AFEventParamContentType: "category_a", AFEventParamRevenue: 4, AFEventParamCurrency: "USD"])
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            }
            
        }
        AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: [AFEventParamContentId: "1234567", AFEventParamContentType: "category_a", AFEventParamRevenue: 1, AFEventParamCurrency: "USD"])
        
        if !purchased {
            startTimer()
        }
    }
    
    
    func goBack(){
         self.navigationController!.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getBookmarkedPosts()
        if showAds{
            initAdMobBanner()
        }
    }
    
    func startTimer() {
        appodealAdTimer?.invalidate()
        appodealAdTimer = Timer.scheduledTimer(timeInterval: 30000.0, target: self, selector: #selector(showInterstitialAd), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        appodealAdTimer?.invalidate()
    }
    
    @objc func showInterstitialAd() {
        Appodeal.showAd(AppodealShowStyle.interstitial, rootViewController: self.navigationController)
        AppsFlyerTracker.shared().trackEvent(AFEventLevelAchieved, withValues: [AFEventParamLevel: 9, AFEventParamScore: 100])
    }

 
    func getLatestPosts(_ sender:AnyObject) {
        view.showHUD(view)
        postsArray.removeAll()
        if selectedCategory != menuBookmarkCatName {
            getBlogPosts(1)
        } else {
            for postObject in bookmarkedPostsArray {
                let post = Posts(managedObject: postObject as NSManagedObject?)
                postsArray.append(post)
            }
            
            if (postsArray.count > 0) {
                DispatchQueue.main.async(execute: { () -> Void in
                    
                    self.tableView.reloadData()
                    self.view.hideHUD()
                    self.footerLoadingView.isHidden = true
                    
                })
            } else {
                DispatchQueue.main.async(execute: { () -> Void in
                    self.refreshControl.endRefreshing()
                    self.view.hideHUD()
                    self.footerLoadingView.activityIndicator.isHidden = true
                    self.footerLabel.text = noMorePostsText
                    
                })
            }
        }
        
    }

    override func viewDidAppear(_ animated: Bool) {
    }
    
    private func getBookmarkedPosts() {
        bookmarkedPostsArray.removeAll()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Post")
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            bookmarkedPostsArray = results as! [NSManagedObject]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func getBlogPosts(_ page:Int) {

        let baseURL = URL(string: websiteUrl)
        
        var getPostsURL = URL(string: "wp-json/wp/v2/posts?_embed&page=\(page)", relativeTo: baseURL)
        
        if selectedCategory != "" {
            getPostsURL = URL(string: "wp-json/wp/v2/posts?_embed&filter[category_name]=\(selectedCategory)&page=\(page)", relativeTo: baseURL)
        }
        
        let sessionConfig = URLSessionConfiguration.default
        let sharedSession = URLSession(configuration: sessionConfig)
        
        let downloadPosts: URLSessionDownloadTask = sharedSession.downloadTask(with: getPostsURL!) { (location, response, error) -> Void in
            
            if (error == nil) {
                
                guard let url = location else {  self.refreshControl.endRefreshing(); self.showErrorMessage(); return }
                guard let dataObject = try? Data(contentsOf: url) else {  self.refreshControl.endRefreshing(); self.showErrorMessage(); return }
                guard let httpHeaders = response as? HTTPURLResponse else {  self.refreshControl.endRefreshing(); self.showErrorMessage(); return }
                guard let headerDict = httpHeaders.allHeaderFields as Any as? NSDictionary else {  self.refreshControl.endRefreshing(); self.showErrorMessage(); return }
                guard let myTotalPostsString: String = headerDict["X-WP-Total"] as? String else {  self.refreshControl.endRefreshing(); self.showErrorMessage(); return }
                guard let myTotalPagesString: String = headerDict["X-WP-TotalPages"] as? String else {  self.refreshControl.endRefreshing(); self.showErrorMessage(); return }
                
                self.totalPosts = Int(myTotalPostsString) ?? 0
                self.totalPages = Int(myTotalPagesString) ?? 0
                UserDefaults.standard.set(self.totalPages, forKey: "totalPages")
                
                guard let blogPostArray: NSArray = (try! JSONSerialization.jsonObject(with: dataObject, options: [])) as? NSArray else { return }
                
                if (blogPostArray.count > 0) {
                    for blogPost in blogPostArray as! [NSDictionary] {
                        let newPost = Posts(post: blogPost)
                        if self.postsArray.count == 0 {
                            UserDefaults.standard.set(newPost.postID, forKey: "lastSeenLatestNewsID")
                        }
                        self.postsArray.append(newPost)
                    }
                    
                    DispatchQueue.main.async(execute: { () -> Void in

                        self.tableView.reloadData()
                        self.view.hideHUD()
                        self.footerLoadingView.isHidden = true
                        
                    })
                } else {
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.refreshControl.endRefreshing()
                        self.footerLoadingView.activityIndicator.isHidden = true
                        self.footerLabel.text = noMorePostsText
                        
                    })
                }
            }
            else {
                
                self.refreshControl.endRefreshing()
                self.showErrorMessage()
                
            }
            
            }
        
        downloadPosts.resume()
        
        
    }

    func showErrorMessage() {
        
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        messageLabel.text = noConnectionText
        messageLabel.textColor = UIColor.darkGray
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font.withSize(20)
        messageLabel.sizeToFit()
        
        DispatchQueue.main.async(execute: { () -> Void in
            self.tableView.backgroundView = messageLabel
        })
        
    }
    
    
    func loadMorePosts() {
        
        self.footerLoadingView.isHidden = false
        self.footerLabel.text = loadingPostsText
        
        if currentPage < totalPages {
            currentPage = currentPage + 1
            getBlogPosts(currentPage)
            
        } else {
            
            DispatchQueue.main.async(execute: { () -> Void in
                self.refreshControl.endRefreshing()
                self.footerLoadingView.activityIndicator.isHidden = true
                self.footerLabel.text = noMorePostsText
                
            })
        }
    }
    

    
    
    @IBAction func btnMenu(_ sender: AnyObject) {
        
        self.view.hideHUD()
        
        if menuView.isHidden {
        
            menuView.animation = "slideRight"
            menuView.duration = 0.5
            menuView.animate()
            menuView.isHidden = false
            menuView.alpha = 0.96
            btnMask.alpha = 0.4
            btnMask.isHidden = false

        } else {
        
            closeMenu()
        
        }
        
    }
    
    @IBAction func btnRefresh(_ sender: AnyObject) {
        getNews()
    }
    
    
    
    @IBAction func btnMask(_ sender: AnyObject) {
        closeMenu()
    }
    
    
    func closeMenu(){
        
        menuView.animation = "fadeOut"
        menuView.duration = 0.5
        menuView.animate()
        
        UIView.animate(withDuration: 0.3, animations: {
            self.btnMask.alpha = 0
            self.menuView.alpha = 0
        })
        
        let delay = 0.2 * Double(NSEC_PER_SEC)
        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time) {
            self.menuView.isHidden = true
            self.btnMask.isHidden = true
        }

    }
    
    // ------------------------------------
    // MARK: - Menu
    // ------------------------------------
    
    @IBAction func btnHome(_ sender: AnyObject) {
        closeMenu()
        title = homeShow
        selectedCategory = ""
        getNews()
    }
    
    
    @IBAction func btnCat1(_ sender: AnyObject) {
        closeMenu()
        title = menu1show
        selectedCategory = menu1catName
        getNews()
    }
    
    @IBAction func btnCat2(_ sender: AnyObject) {
        closeMenu()
        title = menu2show
        selectedCategory = menu2catName
        getNews()
    }
    
    @IBAction func btnCat3(_ sender: AnyObject) {
        closeMenu()
        title = menu3show
        selectedCategory = menu3catName
        getNews()
    }
    
    @IBAction func btnCat4(_ sender: AnyObject) {
        closeMenu()
        title = menu4show
        selectedCategory = menu4catName
        getNews()
    }
    
    @IBAction func btnCat5(_ sender: AnyObject) {
        closeMenu()
        title = menu5show
        selectedCategory = menu5catName
        getNews()
    }
    
    @IBAction func btnCat6(_ sender: AnyObject) {
        closeMenu()
        title = menu6show
        selectedCategory = menu6catName
        getNews()
    }
    
    @IBAction func btnCat7(_ sender: AnyObject) {
        closeMenu()
        title = menu7show
        selectedCategory = menu7catName
        getNews()
    }
    
    @IBAction func btnCat8(_ sender: AnyObject) {
        closeMenu()
        title = menu8show
        selectedCategory = menu8catName
        getNews()
    }
    
    @IBAction func btnCat9(_ sender: AnyObject) {
        closeMenu()
        title = menu9show
        selectedCategory = menu9catName
        getNews()
    }
    
    @IBAction func btnCat10(_ sender: Any) {
        closeMenu()
        IAPHandler.shared.purchaseMyProduct(index: 0)
    }
    // MARK: - RESTORE NON-CONSUMABLE PURCHASE BUTTON
    @IBAction func btnCat11(_ sender: Any) {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    @IBAction func btnCat12(_ sender: Any) {
        closeMenu()
        if let url = URL(string: "https://itunes.apple.com/us/developer/igor-urbitskiy/id1204891029")
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func bookmarkBtnTapped(_ sender: Any) {
        closeMenu()
        title = menuBookmarkShow
        selectedCategory = menuBookmarkCatName
        getNews()
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        
        UserDefaults.standard.set(true, forKey: "cryptoptionsID")
        purchased = true
        stopTimer()
        UIAlertView(title: "IAP Restored.",
                    message: "You've successfully restored your purchase!",
                    delegate: nil, cancelButtonTitle: "OK").show()
    }
    
    func getNews(){
        
        tableView.alpha = 1
        tableView.setContentOffset(CGPoint.zero, animated:true)
        currentPage = 1
        
        let delay = 0.5 * Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC), execute: { () -> Void in
            self.getLatestPosts("" as AnyObject)
            self.tableView.reloadData()
            UIView.animate(withDuration: 1.0, animations: {
            self.lblLoadingPosts.isHidden = true
            self.tableView.alpha = 1
            })
        })
    }
    
    
    // MARK: - TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.postsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.postCell.rawValue , for: indexPath) as! NewsTableViewCell

        if ((indexPath as NSIndexPath).row == self.postsArray.count - 1) {
                loadMorePosts()
            }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if didAnimateCell[indexPath] == nil || didAnimateCell[indexPath]! == false {
            didAnimateCell[indexPath] = true
            let view = cell.contentView
            
            view.layer.opacity = 0.6
            UIView.animate(withDuration: 0.4, animations: {
                view.layer.transform = CATransform3DIdentity
                view.layer.opacity = 1
            })
        }
        
        
    let post: Posts = self.postsArray[(indexPath as NSIndexPath).row]
        if let postCell = cell as? NewsTableViewCell {
            postCell.setPost(post)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 520
        } else {
            return 310
        }
    }

    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifiers.showPostDetails.rawValue {
            if let row = (tableView.indexPathForSelectedRow as NSIndexPath?)?.row {
                let destinationController = segue.destination as! PostDetailTableViewController
                destinationController.post = self.postsArray[row]
                if bookmarkedPostsArray.count > 0 {
                    destinationController.postManagedObject = bookmarkedPostsArray.filter { $0.value(forKey: "postID") as? Int == destinationController.post?.postID }[0]
                    if destinationController.postManagedObject != nil {
                        destinationController.isFavorite = true
                    } else {
                        destinationController.isFavorite = false
                    }
                } else {
                    destinationController.isFavorite = false
                }
            }
        }
    }

    
    // ---------------------------------
    // BANNER ADS
    // ---------------------------------
    
    // Initialize Google AdMob banner
    func initAdMobBanner() {
        
        adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: self.view.frame.size.width, height: adHeight))
        adMobBannerView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: adHeight)
        
        adMobBannerView.adUnitID = ADMOB_UNITID
        adMobBannerView.rootViewController = self
        adMobBannerView.delegate = self
        view.addSubview(adMobBannerView)
        
        let request = GADRequest()
        adMobBannerView.load(request)
    }

    // Hide the banner
    func hideBanner(_ banner: UIView) {
        UIView.beginAnimations("hideBanner", context: nil)
        // Hide the banner moving it below the bottom of the screen
        banner.frame = CGRect(x: 0, y: self.view.frame.size.height, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = true
    }
    
    
    // Show the banner
    func showBanner(_ banner: UIView) {
        UIView.beginAnimations("showBanner", context: nil)
        
        // Move the banner on the bottom of the screen
        banner.frame = CGRect(x: 0, y: self.view.frame.size.height - banner.frame.size.height,
                                  width: banner.frame.size.width, height: banner.frame.size.height);
        UIView.commitAnimations()
        banner.isHidden = false
    }
    
    
    // AdMob banner available
    func adViewDidReceiveAd(_ view: GADBannerView) {
        print("AdMob loaded!")
        showBanner(adMobBannerView)
    }
    
    // NO AdMob banner available
    func adView(_ view: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("AdMob Can't load ads right now, they'll be available later \n\(error)")
        hideBanner(adMobBannerView)
    }

    
    func setMenuNames(){
        
        lblWebsiteName.text = websiteName
        btnMenu1.setTitle(menu1show, for: .normal)
        btnMenu2.setTitle(menu2show, for: .normal)
        btnMenu3.setTitle(menu3show, for: .normal)
        btnMenu4.setTitle(menu4show, for: .normal)
        btnMenu5.setTitle(menu5show, for: .normal)
        btnMenu6.setTitle(menu6show, for: .normal)
        btnMenu7.setTitle(menu7show, for: .normal)
        btnMenu8.setTitle(menu8show, for: .normal)
        btnMenu9.setTitle(menu9show, for: .normal)
    }
    
    
    func setColorsDark(){

        self.navigationItem.leftBarButtonItem?.tintColor = yellow
        self.navigationItem.rightBarButtonItem?.tintColor = yellow
        
        viewRoot.backgroundColor = gray1
        tableView.backgroundColor = gray1
        
        lblWebsiteName.textColor = white
        menuView.backgroundColor = gray2
        //btnHome.setTitleColor(yellow, for: .normal)
        btnMenu1.setTitleColor(yellow, for: .normal)
        btnMenu2.setTitleColor(yellow, for: .normal)
        btnMenu3.setTitleColor(yellow, for: .normal)
        btnMenu4.setTitleColor(yellow, for: .normal)
        btnMenu5.setTitleColor(yellow, for: .normal)
        btnMenu6.setTitleColor(yellow, for: .normal)
        btnMenu7.setTitleColor(yellow, for: .normal)
        btnMenu8.setTitleColor(yellow, for: .normal)
        btnMenu9.setTitleColor(yellow, for: .normal)
    }
    
    
    func formatiPhone(){
        
        switch UIScreen.main.bounds.height {
        
        case 480: // iPhone 4
            let topDistance = 6 as CGFloat
            top1.constant = topDistance
            top2.constant = topDistance
            top3.constant = topDistance
            top4.constant = topDistance
            top5.constant = topDistance
            top6.constant = topDistance
            top7.constant = topDistance
            top8.constant = topDistance
            top9.constant = topDistance
            top10.constant = topDistance

            
        case 568: // iPhone 5
            let topDistance = 12 as CGFloat
            top1.constant = topDistance
            top2.constant = topDistance
            top3.constant = topDistance
            top4.constant = topDistance
            top5.constant = topDistance
            top6.constant = topDistance
            top7.constant = topDistance
            top8.constant = topDistance
            top9.constant = topDistance
            top10.constant = topDistance

        default: break
            
        }

        
    }
    
    
    func formatiPad(){
        adHeight = 80
    }
    
    //Appodeal Delegates
    func interstitialDidLoadAdisPrecache(_ precache: Bool){
        NSLog("Interstitial was loaded")
    }
    func interstitialDidFailToLoadAd(){
        NSLog("Interstitial failed to load")
    }
    func interstitialWillPresent(){
        NSLog("Interstitial will present the ad")
    }
    func interstitialDidDismiss(){
        NSLog("Interstitial was closed")
    }
    func interstitialDidClick(){
        NSLog("Interstitial was clicked")
    }
    
    deinit {
        stopTimer()
    }
    
}
