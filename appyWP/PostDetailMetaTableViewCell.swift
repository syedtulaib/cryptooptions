import UIKit

class PostDetailMetaTableViewCell: UITableViewCell {

    @IBOutlet var postTitleLabel: UILabel!
    @IBOutlet var postDetailsLabel: UILabel!
    @IBOutlet var viewContent: UIView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            postDetailsLabel.font = UIFont(name: (postDetailsLabel.font?.fontName)!, size: 26)
            postTitleLabel.font = UIFont(name: (postTitleLabel.font?.fontName)!, size: 38)
        }
        
        if useDarkColor {
            viewContent.backgroundColor = gray2
            postDetailsLabel.textColor = yellow
            postTitleLabel.textColor = white
        }
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
