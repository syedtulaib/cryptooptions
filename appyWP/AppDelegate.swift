import UIKit
import CoreData
import Appodeal
import OneSignal
import Firebase
import AppsFlyerLib
import FBSDKLoginKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , AppsFlyerTrackerDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //Appodeal
        let adTypes: AppodealAdType = [.interstitial, .banner]
//        fee50c333ff3825fd6ad6d38cff78154de3025546d47a84f
        Appodeal.initialize(withApiKey: "76e5bfe893238927391c9e8054eea7b0124acaacffd8b5f8", types:  adTypes)
        
        //OneSignal
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "02b1ebec-4f6e-4564-a323-e16b299858fd",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        // Sync hashed email if you have a login system or collect it.
        //   Will be used to reach the user at the most optimal time of day.
        // OneSignal.syncHashedEmail(userEmail)
        
        //Firebase
        //AddGoogleService-Info.plist in your project
        FirebaseApp.configure()
        
        //AppsFlyer
//        AppsFlyerTracker.shared().appsFlyerDevKey = "aSikdJAsaAJS123ad23"
        AppsFlyerTracker.shared().appsFlyerDevKey = "Cb84BpRLyB5r2M9m8zjhfe"
        AppsFlyerTracker.shared().appleAppID = "1348050248"
        AppsFlyerTracker.shared().delegate = self
        
        #if DEBUG
            AppsFlyerTracker.shared().isDebug = true
        #endif

        //Facebook
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // Custom Navigation Bar
        
        UINavigationBar.appearance().barTintColor = UIColor(red: (253/255.0), green: (143/255.0), blue: (36/255.0), alpha: 1.0)
        if useDarkColor {
        UINavigationBar.appearance().barTintColor = gray1
        }
        
        
        UINavigationBar.appearance().tintColor = UIColor.white
        
        let font = UIFont(name: "Avenir-Medium", size: 17)
        
        if let font = font {
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: UIColor.white]
        }

        let dataFetchTimeInterval: TimeInterval = 1800 //30 min
        UIApplication.shared.setMinimumBackgroundFetchInterval(dataFetchTimeInterval)
        return true
    }
    
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Track Installs, updates & sessions(app opens) (You must include this API to enable tracking)
        AppsFlyerTracker.shared().trackAppLaunch()
        // your other code here....
        
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let totalPages = UserDefaults.standard.integer(forKey: "totalPages")
        let lastSeenLatestNewsID = UserDefaults.standard.integer(forKey: "lastSeenLatestNewsID")
        if totalPages > 0 && lastSeenLatestNewsID > 0 {
            let unreadCount = getBlogPosts(totalPages, lastSeenLatestNewsID: lastSeenLatestNewsID)
            if unreadCount > 0 {
                UIApplication.shared.applicationIconBadgeNumber = unreadCount
                completionHandler(.newData)
            } else {
                completionHandler(.noData)
            }
        } else {
            completionHandler(.noData)
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        
        return FBSDKApplicationDelegate.sharedInstance().application(
            app,
            open: url,
            sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String,
            annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
    
    func getBlogPosts(_ totalPages: Int, lastSeenLatestNewsID: Int) -> Int {
        let baseURL = URL(string: websiteUrl)
        var totalUnreadNewsCount = 0
        var matchFound = false
        
        for index in 0..<totalPages {
            let getPostsURL = URL(string: "wp-json/wp/v2/posts?_embed&page=\(index)", relativeTo: baseURL)
            let sessionConfig = URLSessionConfiguration.default
            let sharedSession = URLSession(configuration: sessionConfig)
            
            let downloadPosts: URLSessionDownloadTask = sharedSession.downloadTask(with: getPostsURL!) { (location, response, error) -> Void in
                
                if (error == nil) {
                    
                    if let url = location, let dataObject = try? Data(contentsOf: url) {
                        guard let blogPostArray: NSArray = (try! JSONSerialization.jsonObject(with: dataObject, options: [])) as? NSArray else { return }
                        
                        if (blogPostArray.count > 0) {
                            for blogPost in blogPostArray as! [NSDictionary] {
                                let newPost = Posts(post: blogPost)
                                if let postID = newPost.postID {
                                    if postID != lastSeenLatestNewsID {
                                        totalUnreadNewsCount += 1
                                    } else {
                                        matchFound = true
                                        break
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
            downloadPosts.resume()
            
            if matchFound {
                break
            }
        }
        return totalUnreadNewsCount
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "appyWP")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

