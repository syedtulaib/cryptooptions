import UIKit



// Colors for Dark Theme

let gray1 = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
let gray2 = UIColor(red: 68/255, green: 68/255, blue: 68/255, alpha: 1)
let yellow = UIColor(red: 248/255, green: 231/255, blue: 28/255, alpha: 1)
let white = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)




extension String {
    func htmlEncodedString() -> String?  {
        guard let encodedData = self.data(using: String.Encoding.utf8) else {
            return nil
        }
        
        let attributedOptions : [NSAttributedString.DocumentReadingOptionKey: Any] = [
            NSAttributedString.DocumentReadingOptionKey(rawValue: NSAttributedString.DocumentAttributeKey.documentType.rawValue): NSAttributedString.DocumentType.html,
            NSAttributedString.DocumentReadingOptionKey(rawValue: NSAttributedString.DocumentAttributeKey.characterEncoding.rawValue): String.Encoding.utf8.rawValue
        ]
        var attributedString: NSAttributedString?
        do {
            attributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
        } catch let error as NSError {
            print(error)
        }
        
        return attributedString?.string
    }
}





func toFormattedString(_ dateString:String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    
    if let date = dateFormatter.date(from: dateString) {
        dateFormatter.locale = Locale.current
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.timeStyle = .none
        let myDate = dateFormatter.string(from: date)
        
        let formattedDate = String(myDate.characters.map {
            $0 == "/" ? "." : $0
        })
        return formattedDate
    }
    
    return ""
}


// HUD View

let hudView = UIView(frame: CGRect(x: 0, y: 0, width: 160, height: 80))
let indicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
let labelView = UILabel(frame: CGRect(x: 0,y: 0,width: 140,height: 20))
extension UIView {
    
    func showHUD(_ view: UIView) {
        hudView.center = CGPoint(x: view.frame.size.width/2, y: view.frame.size.height/4)
        hudView.backgroundColor = UIColor(red: (0/255.0), green: (0/255.0), blue: (0/255.0), alpha: 0.6)
        hudView.alpha = 0.9
        hudView.layer.cornerRadius = 5
        
        indicatorView.center = CGPoint(x: hudView.frame.size.width/2, y: hudView.frame.size.height/2.4)
        indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
        hudView.addSubview(indicatorView)
        indicatorView.startAnimating()
        
        labelView.center = CGPoint(x: hudView.frame.size.width/2, y: hudView.frame.size.height/1.3)
        labelView.font = UIFont(name: "Avenir", size: 13)
        labelView.textColor = UIColor(red: (220/255.0), green: (220/255.0), blue: (220/255.0), alpha: 1.0)
        labelView.text = loadingPostsText
        labelView.textAlignment = .center
        hudView.addSubview(labelView)
        
        view.addSubview(hudView)
    }
    
    
    func hideHUD() {
        
        UIView.animate(withDuration: 0.4, animations: {
            hudView.alpha = 0
        })
        let delay = 0.5 * Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC), execute: { () -> Void in
            hudView.removeFromSuperview()
        })
        
    }
}

enum CellIdentifiers: String {
    case postCell
}
enum SegueIdentifiers: String {
    case showPostDetails
}
enum Errors : Error {
    case couldNotConnect, unknown
}

