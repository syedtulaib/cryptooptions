//
//  NewsCollectionViewCell.swift
//  appyWP
//
//  Created by Taskeen Ashraf on 2/24/18.
//  Copyright © 2018 Focused on Fit LLC. All rights reserved.
//

import UIKit
import Kingfisher

class NewsCollectionViewCell: UICollectionViewCell {
    
//    @IBOutlet var mainView: UIView!
//    @IBOutlet var contView: UIView!
    @IBOutlet var postDateLabel: UILabel!
    @IBOutlet var postTitleLabel: UILabel!
    @IBOutlet var featuredImage: UIImageView!
    @IBOutlet var viewContainer: UIView!
    
//    @IBOutlet var heightImage: NSLayoutConstraint!
    
    
    func setPost(_ post:Posts) {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            
//            heightImage.constant = 380
            postDateLabel.font = UIFont(name: (postDateLabel.font.fontName), size: 26)
            postTitleLabel.font = UIFont(name: (postTitleLabel.font.fontName), size: 32)
        }
        
        if useDarkColor {
//            contView.backgroundColor = gray1
//            mainView.backgroundColor = gray1
            viewContainer.backgroundColor = gray1
            postDateLabel.textColor = yellow
            postTitleLabel.textColor = white
        }
        
//        mainView.layer.cornerRadius = 2
//        mainView.layer.masksToBounds = true
        postTitleLabel.text = post.postTitle?.htmlEncodedString()
        
        let date = post.postDate
        postDateLabel.text = "\(toFormattedString(date!))"
        
        if let stringURL = post.postFeaturedImageURL {
            let escapedString = stringURL.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
            
            if let url = URL(string: escapedString!)  {
                self.featuredImage.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Placeholder"), options: [], progressBlock: nil, completionHandler: nil)
            } else {
                self.featuredImage.image = UIImage(named:"Placeholder")
            }
            
        } else {
            self.featuredImage.image = UIImage(named:"Placeholder")
        }
    }
}
