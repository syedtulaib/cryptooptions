//
//  NewsCollectionViewController.swift
//  appyWP
//
//  Created by Taskeen Ashraf on 2/24/18.
//  Copyright © 2018 Focused on Fit LLC. All rights reserved.
//

import Foundation
import UIKit
import GoogleMobileAds
import Appodeal
import AppsFlyerLib

class NewsCollectionViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var menuView: SpringView!
    @IBOutlet var btnMask: UIButton!
    @IBOutlet var lblWebsiteName: UILabel!
//    @IBOutlet var activity: UIActivityIndicatorView!
    
//    @IBOutlet var btnHome: UIButton!
    @IBOutlet var btnMenu1: UIButton!
    @IBOutlet var btnMenu2: UIButton!
    @IBOutlet var btnMenu3: UIButton!
    @IBOutlet var btnMenu4: UIButton!
    @IBOutlet var btnMenu5: UIButton!
    @IBOutlet var btnMenu6: UIButton!
    @IBOutlet var btnMenu7: UIButton!
    @IBOutlet var btnMenu8: UIButton!
    @IBOutlet var btnMenu9: UIButton!
    @IBOutlet var btnMenu10: UIButton!
    
    var postsArray: [Posts] = Array()
    var currentPage: Int = 1
    let refreshControl = UIRefreshControl()
    var totalPosts: Int = 0
    var totalPages: Int = 0
    var selectedCategory = ""
    var adMobBannerView = GADBannerView()
    var adHeight = 50 as CGFloat
    var purchased = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        view.backgroundColor = gray1
        
        //Check if IAP
        if let cryptoptionsID = UserDefaults.standard.value(forKey: "cryptoptionsID"){
            purchased = cryptoptionsID as! Bool
        }else{
            UserDefaults.standard.set(false, forKey: "cryptoptionsID")
            purchased = false
        }
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        title = "News"
        
        menuView.layer.zPosition = 10
        setMenuNames()
//
//        if UIDevice.current.userInterfaceIdiom != .pad  {
//            formatiPhone()
//        } else {
//            formatiPad()
//        }
//
        if useDarkColor {
            setColorsDark()
        }
        
        getBlogPosts(currentPage)
        view.showHUD(view)
        
        IAPHandler.shared.fetchAvailableProducts()
        IAPHandler.shared.purchaseStatusBlock = {[weak self] (type) in
            guard let strongSelf = self else{ return }
            if type == .purchased {
                let alertView = UIAlertController(title: "", message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    UserDefaults.standard.set(true, forKey: "cryptoptionsID")
                    self?.purchased = true

                    AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: [AFEventParamContentId: "1234567", AFEventParamContentType: "category_a", AFEventParamRevenue: 4, AFEventParamCurrency: "USD"])
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            }

        }
        AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: [AFEventParamContentId: "1234567", AFEventParamContentType: "category_a", AFEventParamRevenue: 1, AFEventParamCurrency: "USD"])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if !purchased {
            Appodeal.showAd(AppodealShowStyle.interstitial, rootViewController: self.navigationController)
            AppsFlyerTracker.shared().trackEvent(AFEventLevelAchieved, withValues: [AFEventParamLevel: 9, AFEventParamScore: 100])


        }
        if showAds{
            initAdMobBanner()
        }
    }
    
    
    func getLatestPosts(_ sender:AnyObject) {
        view.showHUD(view)
        postsArray.removeAll()
        getBlogPosts(1)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifiers.showPostDetails.rawValue, let post = sender as? Posts {
            let destinationController = segue.destination as! PostDetailTableViewController
            destinationController.post = post
        }
    }
    
    func getBlogPosts(_ page:Int) {
        
        let baseURL = URL(string: websiteUrl)
        
        var getPostsURL = URL(string: "wp-json/wp/v2/posts?_embed&page=\(page)", relativeTo: baseURL)
        
        if selectedCategory != "" {
            getPostsURL = URL(string: "wp-json/wp/v2/posts?_embed&filter[category_name]=\(selectedCategory)&page=\(page)", relativeTo: baseURL)
        }
        
        let sessionConfig = URLSessionConfiguration.default
        let sharedSession = URLSession(configuration: sessionConfig)
        
        let downloadPosts: URLSessionDownloadTask = sharedSession.downloadTask(with: getPostsURL!) { (location, response, error) -> Void in
            
            if (error == nil) {
                
                guard let url = location else {  self.refreshControl.endRefreshing(); self.showErrorMessage(); return }
                guard let dataObject = try? Data(contentsOf: url) else {  self.refreshControl.endRefreshing(); self.showErrorMessage(); return }
                guard let httpHeaders = response as? HTTPURLResponse else {  self.refreshControl.endRefreshing(); self.showErrorMessage(); return }
                guard let headerDict = httpHeaders.allHeaderFields as Any as? NSDictionary else {  self.refreshControl.endRefreshing(); self.showErrorMessage(); return }
                guard let myTotalPostsString: String = headerDict["X-WP-Total"] as? String else {  self.refreshControl.endRefreshing(); self.showErrorMessage(); return }
                guard let myTotalPagesString: String = headerDict["X-WP-TotalPages"] as? String else {  self.refreshControl.endRefreshing(); self.showErrorMessage(); return }
                
                self.totalPosts = Int(myTotalPostsString) ?? 0
                self.totalPages = Int(myTotalPagesString) ?? 0
                
                guard let blogPostArray: NSArray = (try! JSONSerialization.jsonObject(with: dataObject, options: [])) as? NSArray else { return }
                
                if (blogPostArray.count > 0) {
                    for blogPost in blogPostArray as! [NSDictionary] {
                        let newPost = Posts(post: blogPost)
                        self.postsArray.append(newPost)
                    }
                    
                    DispatchQueue.main.async(execute: { () -> Void in
                        
                        self.collectionView.reloadData()
                        self.view.hideHUD()
//                        self.footerLoadingView.isHidden = true
                        
                    })
                } else {
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.refreshControl.endRefreshing()
//                        self.footerLoadingView.activityIndicator.isHidden = true
//                        self.footerLabel.text = noMorePostsText
                        
                    })
                }
            }
            else {
                
                self.refreshControl.endRefreshing()
                self.showErrorMessage()
            }
        }
        downloadPosts.resume()
    }
    
    func loadMorePosts() {
        
//        self.footerLoadingView.isHidden = false
//        self.footerLabel.text = loadingPostsText
        
        if currentPage < totalPages {
            currentPage = currentPage + 1
            getBlogPosts(currentPage)
            
        } else {
            
            DispatchQueue.main.async(execute: { () -> Void in
                self.refreshControl.endRefreshing()
//                self.footerLoadingView.activityIndicator.isHidden = true
//                self.footerLabel.text = noMorePostsText
                
            })
        }
    }
    
    func showErrorMessage() {
        
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        messageLabel.text = noConnectionText
        messageLabel.textColor = UIColor.darkGray
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font.withSize(20)
        messageLabel.sizeToFit()
        
        DispatchQueue.main.async(execute: { () -> Void in
//            self.tableView.backgroundView = messageLabel
        })
        
    }
    
    func setMenuNames(){
        lblWebsiteName.text = websiteName
        btnMenu1.setTitle(menu1show, for: .normal)
        btnMenu2.setTitle(menu2show, for: .normal)
        btnMenu3.setTitle(menu3show, for: .normal)
        btnMenu4.setTitle(menu4show, for: .normal)
        btnMenu5.setTitle(menu5show, for: .normal)
        btnMenu6.setTitle(menu6show, for: .normal)
        btnMenu7.setTitle(menu7show, for: .normal)
        btnMenu8.setTitle(menu8show, for: .normal)
        btnMenu9.setTitle(menu9show, for: .normal)
    }
    
    
    func setColorsDark(){
        self.navigationItem.leftBarButtonItem?.tintColor = yellow
        self.navigationItem.rightBarButtonItem?.tintColor = yellow
        
//        viewRoot.backgroundColor = gray1
        collectionView.backgroundColor = gray1
        
        lblWebsiteName.textColor = white
        menuView.backgroundColor = gray2
        //btnHome.setTitleColor(yellow, for: .normal)
        btnMenu1.setTitleColor(yellow, for: .normal)
        btnMenu2.setTitleColor(yellow, for: .normal)
        btnMenu3.setTitleColor(yellow, for: .normal)
        btnMenu4.setTitleColor(yellow, for: .normal)
        btnMenu5.setTitleColor(yellow, for: .normal)
        btnMenu6.setTitleColor(yellow, for: .normal)
        btnMenu7.setTitleColor(yellow, for: .normal)
        btnMenu8.setTitleColor(yellow, for: .normal)
        btnMenu9.setTitleColor(yellow, for: .normal)
    }
    
    func getNews(){
        
        collectionView.alpha = 1
        collectionView.setContentOffset(CGPoint.zero, animated:true)
        currentPage = 1
        
        let delay = 0.5 * Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC), execute: { () -> Void in
            self.getLatestPosts("" as AnyObject)
            self.collectionView.reloadData()
            UIView.animate(withDuration: 1.0, animations: {
//                self.lblLoadingPosts.isHidden = true
                self.collectionView.alpha = 1
            })
        })
    }
    
    @IBAction func btnMenu(_ sender: AnyObject) {
        
        self.view.hideHUD()
        
        if menuView.isHidden {
            
            menuView.animation = "slideRight"
            menuView.duration = 0.5
            menuView.animate()
            menuView.isHidden = false
            menuView.alpha = 0.96
            btnMask.alpha = 0.4
            btnMask.isHidden = false
            
        } else {
            
            closeMenu()
            
        }
        
    }
    
    @IBAction func btnRefresh(_ sender: AnyObject) {
        getNews()
    }
    
    
    
    @IBAction func btnMask(_ sender: AnyObject) {
        closeMenu()
    }
    
    
    func closeMenu(){
        
        menuView.animation = "fadeOut"
        menuView.duration = 0.5
        menuView.animate()
        
        UIView.animate(withDuration: 0.3, animations: {
            self.btnMask.alpha = 0
            self.menuView.alpha = 0
        })
        
        let delay = 0.2 * Double(NSEC_PER_SEC)
        let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time) {
            self.menuView.isHidden = true
            self.btnMask.isHidden = true
        }
        
    }
    
    // ------------------------------------
    // MARK: - Menu
    // ------------------------------------
    
    @IBAction func btnHome(_ sender: AnyObject) {
        closeMenu()
        title = homeShow
        selectedCategory = ""
        getNews()
    }
    
    
    @IBAction func btnCat1(_ sender: AnyObject) {
        closeMenu()
        title = menu1show
        selectedCategory = menu1catName
        getNews()
    }
    
    @IBAction func btnCat2(_ sender: AnyObject) {
        closeMenu()
        title = menu2show
        selectedCategory = menu2catName
        getNews()
    }
    
    @IBAction func btnCat3(_ sender: AnyObject) {
        closeMenu()
        title = menu3show
        selectedCategory = menu3catName
        getNews()
    }
    
    @IBAction func btnCat4(_ sender: AnyObject) {
        closeMenu()
        title = menu4show
        selectedCategory = menu4catName
        getNews()
    }
    
    @IBAction func btnCat5(_ sender: AnyObject) {
        closeMenu()
        title = menu5show
        selectedCategory = menu5catName
        getNews()
    }
    
    @IBAction func btnCat6(_ sender: AnyObject) {
        closeMenu()
        title = menu6show
        selectedCategory = menu6catName
        getNews()
    }
    
    @IBAction func btnCat7(_ sender: AnyObject) {
        closeMenu()
        title = menu7show
        selectedCategory = menu7catName
        getNews()
    }
    
    @IBAction func btnCat8(_ sender: AnyObject) {
        closeMenu()
        title = menu8show
        selectedCategory = menu8catName
        getNews()
    }
    
    @IBAction func btnCat9(_ sender: AnyObject) {
        closeMenu()
        title = menu9show
        selectedCategory = menu9catName
        getNews()
    }
    
    @IBAction func btnCat10(_ sender: Any) {
        closeMenu()
        IAPHandler.shared.purchaseMyProduct(index: 0)
    }
    // MARK: - RESTORE NON-CONSUMABLE PURCHASE BUTTON
    @IBAction func btnCat11(_ sender: Any) {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    @IBAction func btnCat12(_ sender: Any) {
        closeMenu()
        if let url = URL(string: "https://itunes.apple.com/us/developer/igor-urbitskiy/id1204891029")
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}

extension NewsCollectionViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return postsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCell", for: indexPath) as? NewsCollectionViewCell else {
            fatalError("unable to dequeue cell")
        }
        cell.setPost(postsArray[indexPath.row])
        if indexPath.row == self.postsArray.count - 2 {
            loadMorePosts()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: SegueIdentifiers.showPostDetails.rawValue, sender: postsArray[indexPath.row])
    }
}

extension NewsCollectionViewController: GADBannerViewDelegate,AppodealInterstitialDelegate {
    // ---------------------------------
    // BANNER ADS
    // ---------------------------------
    
    // Initialize Google AdMob banner
    func initAdMobBanner() {
        
        adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: self.view.frame.size.width, height: adHeight))
        adMobBannerView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: adHeight)
        
        adMobBannerView.adUnitID = ADMOB_UNITID
        adMobBannerView.rootViewController = self
        adMobBannerView.delegate = self
        view.addSubview(adMobBannerView)
        
        let request = GADRequest()
        adMobBannerView.load(request)
    }
    
    // Hide the banner
    func hideBanner(_ banner: UIView) {
        UIView.beginAnimations("hideBanner", context: nil)
        // Hide the banner moving it below the bottom of the screen
        banner.frame = CGRect(x: 0, y: self.view.frame.size.height, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = true
    }
    
    
    // Show the banner
    func showBanner(_ banner: UIView) {
        UIView.beginAnimations("showBanner", context: nil)
        
        // Move the banner on the bottom of the screen
        banner.frame = CGRect(x: 0, y: self.view.frame.size.height - banner.frame.size.height,
                              width: banner.frame.size.width, height: banner.frame.size.height);
        UIView.commitAnimations()
        banner.isHidden = false
    }
    
    
    // AdMob banner available
    func adViewDidReceiveAd(_ view: GADBannerView) {
        print("AdMob loaded!")
        showBanner(adMobBannerView)
    }
    
    // NO AdMob banner available
    func adView(_ view: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("AdMob Can't load ads right now, they'll be available later \n\(error)")
        hideBanner(adMobBannerView)
    }
}
