

import UIKit
import WebKit
import GoogleMobileAds
import Appodeal
import AppsFlyerLib
import CoreData

class PostDetailTableViewController: UITableViewController, WKNavigationDelegate, GADBannerViewDelegate, AppodealInterstitialDelegate {

    var post:Posts?
    var postManagedObject: NSManagedObject?
    var footerView = WKWebView()
    var contentString = "Could not load content"
    var shareBarButton = UIBarButtonItem()
    var favoriteButton = UIBarButtonItem()
    var cssFile = ""
    var adMobBannerView = GADBannerView()
    var purchased = false
    var isFavorite = false
    
    @IBOutlet var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Check if IAP
        if let cryptoptionsID = UserDefaults.standard.value(forKey: "cryptoptionsID"){
            purchased = cryptoptionsID as! Bool
        }else{
            UserDefaults.standard.set(false, forKey: "cryptoptionsID")
            purchased = false
        }
        
        //Appodeal
//        if !purchased{
//            Appodeal.setInterstitialDelegate(self)
//            Appodeal.showAd(AppodealShowStyle.bannerBottom, rootViewController: self)
//            AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: [AFEventParamContentId: "1234567", AFEventParamContentType: "category_a", AFEventParamRevenue: 0, AFEventParamCurrency: "USD"])
//        }
        
        let favoriteButtonImageName = isFavorite ? "star-selected" : "star-unselected"
        // Setup navigation buttons
        shareBarButton =  UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.action, target: self, action: #selector(PostDetailTableViewController.presentShareSheet))
        favoriteButton = UIBarButtonItem(image: UIImage(named: favoriteButtonImageName), style: .plain, target: self, action: #selector(favButtonTapped))
        navigationItem.rightBarButtonItems = [shareBarButton, favoriteButton]
        
        if UIDevice.current.userInterfaceIdiom != .pad {
            if useDarkColor {
                self.view.backgroundColor = gray2
                myTableView.backgroundColor = gray2
                self.navigationItem.leftBarButtonItem?.tintColor = yellow
                self.navigationItem.rightBarButtonItems?[0].tintColor = yellow
                self.navigationItem.rightBarButtonItems?[1].tintColor = yellow
                cssFile = "defaultDark"
            } else {
                cssFile = "default"
            }
        } else {
            if useDarkColor {
                self.view.backgroundColor = gray2
                myTableView.backgroundColor = gray2
                self.navigationItem.leftBarButtonItem?.tintColor = yellow
                self.navigationItem.rightBarButtonItem?.tintColor = yellow
                cssFile = "defaultiPadDark"
            } else {
                cssFile = "defaultiPad"
            }
        }
        
        // Load CSS to style post content
        if let cssPath = Bundle.main.url(forResource: cssFile, withExtension: "css") {
            do {
                let cssString = try NSString(contentsOf: cssPath, encoding: String.Encoding.utf8.rawValue)
                if let content = post?.postContent {
                    contentString = (cssString as String) + content
                }
                
            } catch _ as NSError {
                
            }
        }

        footerView.loadHTMLString(contentString, baseURL: nil)
        footerView.navigationDelegate = self
        footerView.scrollView.isScrollEnabled = false
        footerView.frame.size.height = view.frame.size.height
        tableView.tableFooterView = footerView
        tableView.estimatedRowHeight = 100.0
        navigationController?.navigationBar.isTranslucent = false

        
    }

    override func viewWillAppear(_ animated: Bool) {

        if !purchased{
            Appodeal.showAd(AppodealShowStyle.bannerBottom, rootViewController: self.navigationController)
            AppsFlyerTracker.shared().trackEvent(AFEventLevelAchieved, withValues: [AFEventParamLevel: 9, AFEventParamScore: 100])

        }
        if useDarkColor {
            self.view.backgroundColor = gray2
            myTableView.backgroundColor = gray2
            footerView.backgroundColor = gray2
            self.tableView.tableFooterView?.backgroundColor = gray2
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc private func favButtonTapped() {
        if let post = self.post {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let managedContext = appDelegate.persistentContainer.viewContext
            if isFavorite {
                managedContext.delete(postManagedObject!)
            } else {
                let entity =  NSEntityDescription.entity(forEntityName: "Post", in:managedContext)
                let postObject = NSManagedObject(entity: entity!, insertInto: managedContext)
                
                postObject.setValue(post.postID, forKey: "postID")
                postObject.setValue(post.postContent, forKey: "postContent")
                postObject.setValue(post.postDate, forKey: "postDate")
                postObject.setValue(post.postFeaturedImageURL, forKey: "postFeaturedImageURL")
                postObject.setValue(post.postLink, forKey: "postLink")
                postObject.setValue(post.postModified, forKey: "postModified")
                postObject.setValue(post.postTitle, forKey: "postTitle")
                postObject.setValue(post.postAuthor, forKey: "postAuthor")
                postObject.setValue(post.postURL, forKey: "postURL")
            }
            
            do {
                try managedContext.save()
                
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
            
            isFavorite = !isFavorite
            let favoriteButtonImageName = isFavorite ? "star-selected" : "star-unselected"
            favoriteButton.image = UIImage(named: favoriteButtonImageName)
        }
    }

    //MARK: WebKit
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        webView.evaluateJavaScript("document.documentElement.scrollHeight", completionHandler: { (contentHeight, Error) -> Void in
            if let calculatedHeight = contentHeight as? CGFloat {
                self.footerView.frame.size.height = calculatedHeight
            } else {
                self.footerView.frame.size.height = 1500
            }
            
            if useDarkColor {
                self.footerView.backgroundColor = gray2
                self.tableView.tableFooterView?.backgroundColor = gray2
            }
            
            self.tableView.tableFooterView = self.footerView
        })
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .linkActivated {
            guard let url = navigationAction.request.url else {
                 decisionHandler(.cancel)
                return
            }
            UIApplication.shared.openURL(url)
             decisionHandler(.cancel)
            return
        }
            decisionHandler(.allow)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 0
        case 1: return 1
        default: return 0
        }
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0: if let _ = post?.postFeaturedImageURL {
            
            
            if UIDevice.current.userInterfaceIdiom != .pad {
                return 220
            } else {
                return 400
            }

            } else {
                    return 0
                }
        default: return 0
        }
        
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
       
        case 0:
            let reuseIdentifier = "header"
            if let stringURL = post?.postFeaturedImageURL {
                let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as! PostDetailHeaderTableViewCell
                if let url = URL(string: stringURL)  {
                    cell.featuredImage.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "Placeholder"), options: [], progressBlock: nil, completionHandler: nil)
                    cell.featuredImage.contentMode = UIViewContentMode.scaleAspectFill
                    return cell
                }
            }
            // There is no Image
            return nil
            
        default: return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "meta", for: indexPath) as! PostDetailMetaTableViewCell

        cell.postTitleLabel.text = post?.postTitle?.htmlEncodedString()

        guard let date = post?.postDate else {
            cell.postDetailsLabel.text = ""
            return cell
        }
        cell.postDetailsLabel.text = "\(toFormattedString(date))"

        
        
        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
  
   // Share sheet

    @objc func presentShareSheet() {

        var objectsToShare = [AnyObject]()

        // Set Post title
        if let textToShare = post?.postTitle?.htmlEncodedString() {

            objectsToShare.append(textToShare as AnyObject)

        }

        // Set URL
        if let urlString = post?.postURL {
            if let url = URL(string: urlString) {
                objectsToShare.append(url as AnyObject)
            }
        }

        // Set image
        if let stringURL = post?.postFeaturedImageURL {
            if let url = URL(string: stringURL)  {
                let imageView = UIImageView()
                imageView.kf.setImage(with: url, placeholder: nil, options: [], progressBlock: nil, completionHandler: nil)
                if let image = imageView.image {
                    objectsToShare.append(image)
                }

            }
        }

        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

        activityVC.excludedActivityTypes = [UIActivityType.assignToContact, UIActivityType.postToVimeo]
        activityVC.popoverPresentationController?.barButtonItem = shareBarButton
        activityVC.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up

        present(activityVC, animated: true, completion: nil)

    }
    

    // ---------------------------------
    // BANNER ADS
    // ---------------------------------
    
    // Initialize Google AdMob banner
    func initAdMobBanner() {
        
        adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: self.view.frame.size.width, height: 50))
        adMobBannerView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: 50)
        adMobBannerView.adUnitID = ADMOB_UNITID
        adMobBannerView.rootViewController = self
        adMobBannerView.delegate = self
        view.addSubview(adMobBannerView)
        
        let request = GADRequest()
        adMobBannerView.load(request)
    }
    
    // Hide the banner
    func hideBanner(_ banner: UIView) {
        UIView.beginAnimations("hideBanner", context: nil)
        // Hide the banner moving it below the bottom of the screen
        banner.frame = CGRect(x: 0, y: self.view.frame.size.height, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = true
    }
    
    
    // Show the banner
    func showBanner(_ banner: UIView) {
        UIView.beginAnimations("showBanner", context: nil)
        
        // Move the banner on the bottom of the screen
        banner.frame = CGRect(x: 0, y: self.view.frame.size.height - banner.frame.size.height,
                              width: banner.frame.size.width, height: banner.frame.size.height);
        UIView.commitAnimations()
        banner.isHidden = false
    }
    
    
    // AdMob banner available
    func adViewDidReceiveAd(_ view: GADBannerView) {
        print("AdMob loaded!")
        showBanner(adMobBannerView)
    }
    
    // NO AdMob banner available
    func adView(_ view: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("AdMob Can't load ads right now, they'll be available later \n\(error)")
        hideBanner(adMobBannerView)
    }
    
    //Appodeal Delegates
    func interstitialDidLoadAdisPrecache(_ precache: Bool){
        NSLog("Interstitial was loaded")
    }
    func interstitialDidFailToLoadAd(){
        NSLog("Interstitial failed to load")
    }
    func interstitialWillPresent(){
        NSLog("Interstitial will present the ad")
    }
    func interstitialDidDismiss(){
        NSLog("Interstitial was closed")
    }
    func interstitialDidClick(){
        NSLog("Interstitial was clicked")
    }

  
}
